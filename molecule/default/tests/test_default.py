import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_dhcp_installed(host):
    tftp = host.package("dhcp")
    assert tftp.is_installed


def test_dhcp_running(host):
    tftp_service = host.service("dhcpd")
    assert tftp_service.is_running
    assert tftp_service.is_enabled


def test_dhcp_dir(host):
    file = host.file("/etc/dhcp")
    assert file.is_directory
